# Taller Git

Taller de git en el marco del proyecto ICB-FCEN

## Algunos datos piolas...
... que aprendí armando esta presentación

### 1. Hacer gift animados a partir de imágenes:

En una carpeta que contiene todas las imágenes (en este caso en formato PNG):
```bash
  convert  -delay 90 -resize %60 -loop 0 +repage *.png[900x900+0+160] animation.gif
```
donde:

- -delay x        establece el tiempo entre frames (no sé la unidad, pero parece que son centésimas de segundo)
- -resize %x      redimensiona las imágenes
- loop x          número de iteraciones
- +repage         redimensiona el canvas (eso tiene efecto si se recortan las imágenes con lo que sigue abajo..
- *.png[HxW+X+Y]  recorta las imágenes al leerlas. El rectángulo es de HxW y el offset desde arriba a la izquierda es de X-Y
- xxxx.yy         formato de salida. En este caso un GIF animado.


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.